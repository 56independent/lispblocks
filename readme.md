# Lispblocks
**Note** The code is nowhere near a working state. No feature works as intended.

Lispblocks brings one of the purest programming languages to Minetest. It means that automations can be made from a more abstract layer, forcing cleaner code.

## Small note on Lisp
Lisp, from a basic level, is so simple i can describe it using a mostly-short list:

* Everything needed to compute a specific thing goes in brackets
* The first thing is the function name
* The next things are the arguments, which can be any of:
    * strings
    * numbers
        * signed 
        * unsigned
        * integers 
        * floats
    * The return value of a nested function

That's all you need to know, at least for lispblocks's specific dialect. This simplicity is what makes it perfect for readability. You need to know little to make stuff in it, and little to read it.

## Nodes
There are two nodes in lispblocks; the lua-lisp function translator and the lisp interpreter.

### Lua-Lisp translator
The lua-lisp translator contains a luatable defining lisp functions as lua. You do not need to write the table boilerplate, only the items.

```
add = function (x, y)
    return x+y
end,
subtract = function (x, y)
    return x-y
end
```

By default, there is a standard library, which gives you some essential features.

### Lisp Interpreter
The lisp interpreter takes in lisp, queries the lua-lisp translator, and then runs the resultant lua. Here is some example lisp code:

```
(add (subtract 5 2) (add 2 2))
```

The lisp interpreter also gives a more intuitive interface for triggering on events then the luacontroller does.

The lisp interpreter is what executes any IO commands. Connect digilines to it, and not the lua-lisp translator.

## Intended usage
The intended usage for this mod is in complicated scripts. By providing an abstraction layer where it's difficult to be lazy and neglect making functions, it is hoped code like this becomes more natural to default to:

```
(if (less-than (query-machine-demand)  (query-supply))
    ; Yes then this
        (destroy-generation)
    ; Else then this
        (create-generation)
)
```

here, the code will be converted into functions 

