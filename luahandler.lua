local httpblock_nodebox = {
	type = "fixed",
	fixed = {
		{ -8/16, -8/16, -8/16, 8/16, -7/16, 8/16 }, -- bottom slab

		{ -7/16, -7/16, -7/16, 7/16, -5/16,  7/16 },
	}
}

local httpblock_selbox = {
	type = "fixed",
	fixed = {{ -8/16, -8/16, -8/16, 8/16, -3/16, 8/16 }}
}


local on_digiline_receive = function (pos, _, channel, msg)
	local setchan = minetest.get_meta(pos):get_string("channel")
	if channel == setchan and msg == "giveMeCode" then  
		-- Handle the lua inside
		local meta = minetest.get_meta(pos)
		
		if (meta:get_string("code")) then
            code = "functions = {" .. code .. "}"
        	digilines.receptor_send(pos, digilines.rules.default, channel, code)
        end
	end
end

minetest.register_node("lispblocks:luahandler", {
	description = "Lua-Lisp Converter",
	drawtype = "nodebox",
	tiles = {"lispblocks_lisplua.png"},

	paramtype = "light",
	paramtype2 = "facedir",
	groups = {dig_immediate=2},
	selection_box = httpblock_selbox,
	node_box = httpblock_nodebox,
	digilines =
	{
		receptor = {},
		effector = {
			action = on_digiline_receive
		},
	},
	
	on_rightclick = function(pos)
		local meta = minetest.get_meta(pos)
		
		if (meta:get_string("code")) then
		    local code = meta:get_string("code")    
		else
		    local code = [[
            add = function (x, y)
                return x+y
            end,
            subtract = function (x, y)
                return x-y
            end
            ]]
        end

        local formspecTable = table.concat({ -- Using table.concat as per https://rubenwardy.com/minetest_modding_book/en/players/formspecs.html
            "formspec_version[4]",
            "size[12,14]",
            "textarea[1,0.8;10,10;lisp;Enter function definitions (consult readme for details);" .. minetest.formspec_escape(code) .. "]",
            "button[1,11;5,1;save;Save functions]",
            "button[6,11;5,1;execute;Test functions]",
            "field[1,13;4,0.5;mychannel;Channel of code place;]",
        })
		
		meta:set_string("formspec", formspecTable)
	end,

	on_receive_fields = function(pos, _, fields, sender)
		local name = sender:get_player_name()
		if minetest.is_protected(pos, name) and not minetest.check_player_privs(name, {protection_bypass=true}) then
			minetest.record_protection_violation(pos, name)
			return
		end

	    local meta = minetest.get_meta(pos)
        
		if fields.lisp then
		    meta:set_string("code", fields.lisp) -- We'll always save the code no matter what button is set
		end
		
		if fields.channel then
            meta:set_string("channel", fields.channel)
		end

		
	    if (fields.execute) then
	        code = "functions = {" .. code .. "}"
	        
	        -- Run the lua? I don't know how! Maybe i make a mod API.
		end
	end,
})

minetest.register_craft({
	output = "httpblock:httpblock",
	recipe = {
		{"dye:black", "dye:green", "dye:black"},
		{"default:steel_ingot", "default:mese_crystal", "default:steel_ingot"},
		{"", "digilines:wire_std_00000000", ""}
	}
})
