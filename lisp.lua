local http = ...

local httpblock_nodebox = {
	type = "fixed",
	fixed = {
		{ -8/16, -8/16, -8/16, 8/16, -7/16, 8/16 }, -- bottom slab

		{ -7/16, -7/16, -7/16, 7/16, -5/16,  7/16 },
	}
}

local httpblock_selbox = {
	type = "fixed",
	fixed = {{ -8/16, -8/16, -8/16, 8/16, -3/16, 8/16 }}
}

local codeHeader = "" -- Variable to hold the stuff from the luahandler.

local runCode = function (pos, headers)
	digilines.receptor_send(pos, digilines.rules.default, minetest.get_meta(pos):get_string("channel"), "giveMeCode")
	
	code = minetest.get_meta(pos):get_string("lisp")
	
	
end

local on_digiline_receive = function (pos, _, channel, msg)
	local setchan = minetest.get_meta(pos):get_string("channel")
	if channel == setchan then  
		
		codeHeader = msg
		
		-- Handle the lua inside
		if minetest.get_meta(pos):get_bool("digiline") then
		    runCode(pos, codeHeader)
        end
	end
end

minetest.register_node("lispblocks:lisp", {
	description = "Lisp reader",
	drawtype = "nodebox",
	tiles = {"lispblocks_lisp.png"},

	paramtype = "light",
	paramtype2 = "facedir",
	groups = {dig_immediate=2},
	selection_box = httpblock_selbox,
	node_box = httpblock_nodebox,
	digilines =
	{
		receptor = {},
		effector = {
			action = on_digiline_receive
		},
	},

	on_rightclick = function(pos)
		local formspecTable = table.concat({ -- Using table.concat as per https://rubenwardy.com/minetest_modding_book/en/players/formspecs.html
            "formspec_version[4]",
            "size[12,14]",
            "textarea[1,0.8;10,7;lisp;Enter lisp code;(add 2 2)]",
            "button[1,8;5,1;save;Save code]",
            "button[6,8;5,1;execute;Execute code]",
            "checkbox[1,9.5;second;Trigger every 5 seconds;false]",
            "checkbox[1,10;punch;Trigger on punch;true]",
            "checkbox[1,10.5;mesecon;Trigger on mesecon rising edge;false]",
            "checkbox[1,11.0;digiline;Trigger on digigline message;false]",
            "field[1.25,12;4,0.5;channel;Digiline channel;]",
            "field[5.25,12;4,0.5;message;Message string;]",
            "field[1,13;4,0.5;mychannel;Channel of code place;]",
        })
		
		local meta = minetest.get_meta(pos)
		
		meta:set_string("formspec", formspecTable)
	end,

	on_receive_fields = function(pos, _, fields, sender)
		local name = sender:get_player_name()
		if minetest.is_protected(pos, name) and not minetest.check_player_privs(name, {protection_bypass=true}) then
			minetest.record_protection_violation(pos, name)
			return
		end

	    local meta = minetest.get_meta(pos)
        
		if fields.lisp then
		    meta:set_string("code", fields.lisp)
		end
		
		if fields.mychannel then
		    meta:set_string("channel", fields.mychannel)
		end
		
		-- Process the bools
		
		if fields.second then
		    meta:set_bool("timer", fields.second)
		end
		
		if fields.punch then
		    meta:set_bool("punch", fields.punch)
		end
		
		if fields.mesecon then
		    meta:set_bool("mesecon", fields.mesecon)
		end
	
		if fields.digiline and fields.message and fields.mychanneñ then
		    meta:set_bool("digiline", fields.digiline)
		    meta:set_string("eventMessage", fields.message)
		    meta:set_string("eventChannel", fields.channel)
		end
   end
})

minetest.register_craft({
	output = "httpblock:httpblock",
	recipe = {
		{"dye:black", "dye:green", "dye:black"},
		{"default:steel_ingot", "default:mese_crystal", "default:steel_ingot"},
		{"", "digilines:wire_std_00000000", ""}
	}
})
